#pragma once
#include "math.h"

template<typename T, int height, int width>class GMatrix;

namespace gvector{
	template <typename T, const int len> class GVector{
	public:
		inline GVector();
		inline GVector(const GVector& vec);
		inline GVector operator+(const GVector& v1);
		inline GVector operator-(const GVector& v1);
//Not sure why it's so awkward to declare these outside of the class, but it is.
		inline friend GVector operator*(const GVector<T, len>& vector, const T scalar){
			GVector<T, len> res;
			for (int i = 0; i < len; i++){
				res.components[i] = vector.components[i] * scalar;
			}
			return res;
		}

		inline friend GVector operator*(const T scalar, const GVector<T, len>& vector){
			return vector * scalar;
		}

		inline friend GVector operator/(const GVector<T, len>& vector, const T scalar){
			GVector<T, len> res;
			for (int i = 0; i < len; i++){
				res.components[i] = vector.components[i] / scalar;
			}
			return res;
		}

		inline friend GVector operator/(const T scalar, const GVector<T, len>& vector){
			GVector<T, len> res;
			for (int i = 0; i < len; i++){
				res.components[i] = scalar / vector.components[i];
			}
			return res;
		}
//
		inline const GVector operator-();
		inline void operator=(const GVector& vector);
		inline void operator+=(const GVector& vector);
		inline void operator-=(const GVector& vector);
		inline T& operator[](int i);
		inline operator const T*();
		inline static int size();
		inline GVector normal();
		inline double dot(GVector vector);
		static inline double dot(GVector vec1, GVector vec2);
		inline bool equals(const GVector& vector);
	protected:
		T components[len];
		inline void copy(const GVector& vector);
	};
	
	
	template <typename T> class Vec2 : public GVector<T, 2>{
	public:
		inline Vec2();
		inline Vec2(const GVector<T, 2>& vec);
		inline Vec2(T x, T y);
	};

	template <typename T> class Vec3 : public GVector<T, 3>{
	public:
		inline Vec3();
		inline Vec3(const GVector<T, 3>& vec);
		inline Vec3(T x, T y, T z);
	};

	template <typename T> class Vec4 : public GVector<T, 4>{
	public:
		inline Vec4();
		inline Vec4(const GVector<T, 4>& vec);
		inline Vec4(T x, T y, T z, T w);
	};

	typedef Vec2<float> gvec2;
	typedef Vec3<float> gvec3;
	typedef Vec4<float> gvec4;

#include "GVector.inl"
}