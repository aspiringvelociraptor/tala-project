
#include "Matrix.h"
#include "stdafx.h"

Mat3::Mat3(){
	for (int i = 0; i < 9; i++){
		mat[i] = 0;
	}
}

Mat3::Mat3(GLdouble mat[]){
	for (int i = 0; i < 9; i++){
		this->mat[i] = mat[i];
	}
}

Mat3::Mat3(GLdouble r0c0, GLdouble r0c1, GLdouble r0c2,
	GLdouble r1c0, GLdouble r1c1, GLdouble r1c2,
	GLdouble r2c0, GLdouble r2c1, GLdouble r2c2){
	GLdouble mat[] = { r0c0, r0c1, r0c2,
					r1c0, r1c1, r1c2,
					r2c0, r2c1, r2c2 };
}
Mat3 rotationMat(GLdouble angle){
	GLdouble res[9];
	res[0] = res[4] = cos(angle);
	res[1] = -sin(angle);
	res[3] = -res[1];
	res[8] = 1;
	res[2] = res[5] = res[6] = res[7] = 0;
	return Mat3(res);
}

Mat3 translationMat(GLdouble x, GLdouble y){
	GLdouble res[9];
	res[0] = res[4] = res[8] = 1;
	res[2] = x;
	res[5] = y;
	res[1] = res[3] = res[6] = res[7] = 0;
	return Mat3(res);
}

Mat3 scalingMat(GLdouble x, GLdouble y){
	GLdouble res[9];
	res[0] = x;
	res[4] = y;
	res[8] = 1;
	res[1] = res[2] = res[3] = res[5] = res[6] = res[7] = 0;
	return Mat3(res);
}



