
template <typename T, const int len> GVector<T, len>::GVector(){};

template <typename T, const int len> GVector<T, len>::GVector(const GVector& vec){ copy(vec); }

template <typename T, const int len> GVector<T, len> GVector<T, len>::operator+(const GVector& v1){
	GVector<T, len> res;
	for (int i = 0; i < len; i++){
		res.components[i] = components[i] + v1.components[i];
	}
	return res;
}

template <typename T, const int len> GVector<T, len> GVector<T, len>::operator-(const GVector& v1){
	GVector<T, len> res;
	for (int i = 0; i < len; i++){
		res.components[i] = components[i] - v1.components[i];
	}
	return res;
}
/*
template <typename T, const int len> GVector<T, len> operator*(const GVector<T, len>& vector, const T scalar){
	GVector<T, len> res;
	for (int i = 0; i < len; i++){
		res.components[i] = vector.components[i] * scalar;
	}
	return res;
}

template <typename T, const int len> GVector<T, len> operator*(const T scalar, const GVector<T, len>& vector){
	return vector * scalar;
}

template <typename T, const int len> GVector<T, len> operator/(const GVector<T, len>& vector, const T scalar){
	GVector<T, len> res;
	for (int i = 0; i < len; i++){
		res.components[i] = vector.components[i] / scalar;
	}
	return res;
}
*/

template <typename T, const int len> GVector<T, len> const GVector<T, len>::operator-(){
	GVector<T, len> res;
	for (int i = 0; i < len; i++){
		res.components[i] = -components[i];
	}
	return res;
}

template <typename T, const int len> void GVector<T, len>::operator=(const GVector& vector){ copy(vector); }

template <typename T, const int len> void GVector<T, len>::operator+=(const GVector& vector){
	for (int i = 0; i < len; i++){
		components[i] = components[i] + vector.components[i];
	}
}

template <typename T, const int len> void GVector<T, len>::operator-=(const GVector& vector){
	for (int i = 0; i < len; i++){
		components[i] = components[i] - vector.components[i];
	}
}

template <typename T, const int len> T& GVector<T, len>::operator[](int i){
	return components[i];
}

template <typename T, const int len> GVector<T, len>::operator const T*(){ return components; }

template <typename T, const int len> static int GVector<T, len>::size(){ return len; }

template <typename T, const int len> bool GVector<T, len>::equals(const GVector& vector){
	for (int i = 0; i < len; i++){
		if (components[i] != vector.components[i])
			return false;
	}
	return true;
}

template <typename T, const int len> void GVector<T, len>::copy(const GVector& vector){
	for (int i = 0; i < len; i++){
		components[i] = vector.components[i];
	}
}



template <typename T> Vec2<T>::Vec2(){}
template <typename T> Vec2<T>::Vec2(const GVector<T, 2>& vec){ copy(vec); }
template <typename T> Vec2<T>::Vec2(T x, T y){
	GVector<T, 2>::components[0] = x;
	GVector<T, 2>::components[1] = y;
}

template <typename T> Vec3<T>::Vec3(){}
template <typename T> Vec3<T>::Vec3(const GVector<T, 3>& vec){ copy(vec); }
template <typename T> Vec3<T>::Vec3(T x, T y, T z){
	GVector<T, 3>::components[0] = x;
	GVector<T, 3>::components[1] = y;
	GVector<T, 3>::components[2] = z;
}

template <typename T> Vec4<T>::Vec4(){}
template <typename T> Vec4<T>::Vec4(const GVector<T, 4>& vec){ copy(vec); }
template <typename T> Vec4<T>::Vec4(T x, T y, T z, T w){
	GVector<T, 4>::components[0] = x;
	GVector<T, 4>::components[1] = y;
	GVector<T, 4>::components[2] = z;
	GVector<T, 4>::components[3] = w;
}