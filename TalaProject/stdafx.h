// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define __WINDOWS_DS__
#define WINVER 0x0601
#define _WIN32_WINNT 0x0601
// Windows Header Files:
#include <windows.h>
#include <windowsx.h>
#include <ShObjIdl.h>
#include <objbase.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <knownfolders.h>
#include <new>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <strsafe.h>
#include <mmsystem.h>
#include <functional>
#include <vector>
