#pragma once

#include "onsetsds.h"
#include "onsetsdshelpers.h"
#include "sndfile.h"

class AudioFile
{
public:
	AudioFile();
	AudioFile(char* file);
	~AudioFile();
	void initialise(char* file);
	void play();
	void stop();
	int detectOnsets();
	char* getPath();
	static void addOnset(OnsetsDS* ods, double time);

private:
	OnsetsDS ods;
	OnsetsDSAudioBuf odsbuff;
	float* odsdata;
	char file[MAX_PATH];
	std::vector<double> onsets;					//Where we store our onsets
	static std::vector<double> onsetbuffer;		//Our static buffer where we store onsets before copying them
    static void onsetCallback(OnsetsDS* ods, double time);
};

