template<typename T, const int height, const int width> GMatrix<T, height, width>::GMatrix(){}
template<typename T, const int height, const int width> GMatrix<T, height, width>::GMatrix(GVector<T, height>* cols){
	for (int i = 0; i < width; i++){
		this.cols[i] = cols[i];
	}
}
template<typename T, const int height, const int width> GMatrix<T, height, width>::GMatrix(const GMatrix& mat){
	copy(mat);
}

template<typename T, const int height, const int width> GMatrix<T, height, width> GMatrix<T, height, width>::operator+(const GMatrix& mat){
	GMatrix<T, height, width> res;
	for (int i = 0; i < width; i++){
		res.cols[i] = this.cols[i] + mat.cols[i];
	}
	return res;
}
template<typename T, const int height, const int width> GMatrix<T, height, width> GMatrix<T, height, width>::operator-(const GMatrix& mat){
	GMatrix<T, height, width> res;
	for (int i = 0; i < width; i++){
		res.cols[i] = this.cols[i] - mat.cols[i];
	}
	return res;
}
template<typename T, const int height, const int width> GMatrix<T, height, width> GMatrix<T, height, width>::operator=(const GMatrix& right){
	GMatrix<T, height, width> res;
	res.copy(right);
	return res;
}

template<typename T, const int height, const int width> void GMatrix<T, height, width>::copy(const GMatrix& mat){
	for (int i = 0; i < width; i++){
		this.cols[i] = mat.cols[i];
	}
}
