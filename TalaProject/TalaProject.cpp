// TalaProject.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TalaProject.h"
#include "AudioFile.h"
#include "GMatrix.h"
#include <gl\glew.h>
#include <gl\gl.h>	
#include <gl\glu.h>

using namespace gvector;
using namespace gmatrix;

#define MAX_LOADSTRING	100
const int BTNW	= 100;
const int BTNH = 20;
const int MGN = 7;
// Global Variables:
HINSTANCE	hInst;								// current instance
HANDLE		thread;
HWND		hWndGL;
HDC			hDC = NULL;
HGLRC		hRC = NULL;
TCHAR		szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR		szWindowClass[MAX_LOADSTRING];			// the main window class name
TCHAR		audioFilePath[MAX_PATH];
char		filename[MAX_PATH];
RECT		mainSize;
RECT		childrect;
bool		runchild = true;
//
AudioFile a1;

// Forward declarations of functions included in this code module:
int APIENTRY		childThread(HWND);
ATOM				MyRegisterClass(HINSTANCE hInstance);
ATOM				RegisterGLChildWindow();
BOOL				InitGLWindow(HWND);
BOOL				InitInstance(HINSTANCE, int);
HWND				InitPlayButton(HWND);
HWND				InitStopButton(HWND);
HRESULT				OpenFile(HWND);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	PProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void KillGLWindow	(HWND, HINSTANCE);
void renderGL		();
void resizeGL		(int, int);
void updateChildRect(HWND);
//
//File types for IFileDialog
const COMDLG_FILTERSPEC fTypes[] =
{
	{ L"WAV Files (*.wav)", L"*.wav" },
	{ L"All Files (*.*)", L"*.*" }
};

void run();
char out[2];

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.

	gvec4 testa(2, 3, 4, 5);
	gvec4 testb(1, 4, 6, 8);
	gvec4 testc(-testa);
	gvec4 testd = testa*4;
	gvec4 res = testa - testb;

	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TALAPROJECT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TALAPROJECT));

	
	// Main message loop:
	while (GetMessage(&msg, 0, 0, 0) != 0)
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	runchild = false;
	WaitForSingleObject(thread, INFINITE);
	return (int) msg.wParam;
}

int APIENTRY childThread(HWND parent){
	glewInit();
	InitGLWindow(parent);
	glClearColor(0, 0, 0, 0);
	MSG msg;
	while (runchild){
		while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)){
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT || msg.message == WM_DESTROY){
				runchild = false;
			}
		}

		renderGL();
		Sleep(1);
	}
	KillGLWindow(hWndGL, hInst);
	return 0;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TALAPROJECT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TALAPROJECT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd; 

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_VISIBLE,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);


   return TRUE;
}

HWND InitPlayButton(HWND parent){
	HWND hwndButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"Play",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		MGN,         // x position 
		MGN,         // y position 
		BTNW,        // Button width
		BTNH,        // Button height
		parent,     // Parent window
		(HMENU) IDC_PLAY,    
		GetModuleHandle(NULL),
		NULL);      // Pointer not needed.
	return hwndButton;
}

HWND InitStopButton(HWND parent){
	HWND hwndButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		L"Stop",      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		MGN,         // x position 
		BTNH + MGN*2,         // y position 
		BTNW,        // Button width
		BTNH,        // Button height
		parent,     // Parent window
		(HMENU)IDC_STOP,
		GetModuleHandle(NULL),
		NULL);      // Pointer not needed.
	return hwndButton;
}

//Largely taken from the Microsoft IFileDialog example
HRESULT OpenFile(HWND hWnd){
	HRESULT hr = S_OK;

	// Create a new common open file dialog.
	IFileDialog *pfd = NULL;
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pfd));
	if (SUCCEEDED(hr))
	{
		// Control the default folder of the file dialog. Here we set it as the 
		// Music library known folder. 
		IShellItem *psiMusic = NULL;
		hr = SHCreateItemInKnownFolder(FOLDERID_Music, 0, NULL,
			IID_PPV_ARGS(&psiMusic));
		if (SUCCEEDED(hr))
		{
			hr = pfd->SetFolder(psiMusic);
			psiMusic->Release();
		}

		// Set the title of the dialog.
		if (SUCCEEDED(hr))
		{
			hr = pfd->SetTitle(L"Select a File");
		}

		// Specify file types for the file dialog.
		if (SUCCEEDED(hr))
		{
			hr = pfd->SetFileTypes(ARRAYSIZE(fTypes), fTypes);
			if (SUCCEEDED(hr)){
				hr = pfd->SetFileTypeIndex(1);
			}
		}

		// Set the default extension to be added to file names as ".wav"
		if (SUCCEEDED(hr))
		{
			hr = pfd->SetDefaultExtension(L"wav");
		}

		// Show the open file dialog.
		if (SUCCEEDED(hr))
		{
			hr = pfd->Show(hWnd);
			if (SUCCEEDED(hr))
			{
				// Get the result of the open file dialog.
				IShellItem *psiResult = NULL;
				hr = pfd->GetResult(&psiResult);
				if (SUCCEEDED(hr))
				{
					PWSTR pszPath = NULL;
					hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszPath);
					if (SUCCEEDED(hr))
					{
						wcscpy_s(audioFilePath, pszPath);
						CoTaskMemFree(pszPath);
					}
					psiResult->Release();
				}
			}
			else
			{
				if (hr == HRESULT_FROM_WIN32(ERROR_CANCELLED))
				{
					// User cancelled the dialog...
				}
			}
		}

		pfd->Release();	
	}
	return hr;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_OPEN:
			OpenFile(hWnd);
			WideCharToMultiByte(CP_ACP, WC_COMPOSITECHECK, audioFilePath, MAX_PATH, filename, sizeof(filename), NULL, NULL);
			a1.initialise(filename);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_EXPORT:
			break;
		case IDC_PLAY:
			a1.play();
			break;
		case IDC_STOP:
			a1.detectOnsets();
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CREATE:
		thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&childThread, (LPVOID)hWnd, 0, 0);
		InitPlayButton(hWnd);
		InitStopButton(hWnd);
		break;
	case WM_SIZE:
		updateChildRect(hWnd);
		MoveWindow(hWndGL, childrect.left, childrect.top, childrect.right, childrect.bottom, TRUE);
		break;
	case WM_PAINT:
		
		hdc = BeginPaint(hWnd, &ps);

		//Print out file path
		TextOutA(hdc,
			5, 5,
			a1.getPath(), strlen(a1.getPath()));
		TextOutA(hdc,
			5, 50,
			out, strlen(out));
			
		EndPaint(hWnd, &ps);
		
		// TODO: Add any more drawing code here...
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK PProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam){
	switch (message){
	case WM_SIZE:
		updateChildRect(hWnd);
		resizeGL(childrect.right, childrect.bottom);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


//Register the child window of main, which will contain any opengl renders
ATOM RegisterGLChildWindow(){

	WNDCLASS glwc = { 0 };
	glwc.lpszClassName = L"GLChildWindow";
	glwc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	glwc.cbClsExtra = 0;
	glwc.cbWndExtra = 0;
	glwc.hbrBackground = NULL;
	glwc.lpfnWndProc = PProc;
	glwc.hCursor = LoadCursor(NULL, IDC_CROSS);
	glwc.hInstance = GetModuleHandle(NULL);
	glwc.lpszMenuName = NULL;
	return RegisterClassW(&glwc);
}

void KillGLWindow(HWND hWnd, HINSTANCE hInstance){
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL, NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL, L"Release Of DC And RC Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL, L"Release Rendering Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		hRC = NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd, hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL, L"Release Device Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hDC = NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL, L"Could Not Release hWnd.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hWnd = NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass(L"GLChildWindow", hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL, L"Could Not Unregister Class.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;									// Set hInstance To NULL
	}
}

//Create our GL child window
BOOL InitGLWindow(HWND hWnd){
	updateChildRect(hWnd);
	GLuint PixelFormat;
	if (!RegisterGLChildWindow()){
		MessageBox(NULL, L"Failed To Register The GL Child Window.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	if (!(hWndGL = CreateWindowW(L"GLChildWindow", NULL, WS_CHILD | WS_VISIBLE,
		childrect.left, childrect.top, childrect.right, childrect.bottom, hWnd, NULL, GetModuleHandle(NULL), NULL))){
		MessageBox(NULL, L"GLWindow Creation Error.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	static	PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,											// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(hDC = GetDC(hWndGL)))							// Did We Get A Device Context?
	{
		KillGLWindow(hWndGL, hInst);								// Reset The Display
		MessageBox(NULL, L"Can't Create A GL Device Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow(hWndGL, hInst);								// Reset The Display
		MessageBox(NULL, L"Can't Find A Suitable PixelFormat.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!SetPixelFormat(hDC, PixelFormat, &pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow(hWndGL, hInst);								// Reset The Display
		MessageBox(NULL, L"Can't Set The PixelFormat.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(hRC = wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow(hWndGL, hInst);								// Reset The Display
		MessageBox(NULL, L"Can't Create A GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!wglMakeCurrent(hDC, hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow(hWndGL, hInst);								// Reset The Display
		MessageBox(NULL, L"Can't Activate The GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}
	ShowWindow(hWndGL, SW_SHOW);
	resizeGL(childrect.right, childrect.bottom);

}

void renderGL(){
	glClear(GL_COLOR_BUFFER_BIT);
	SwapBuffers(hDC);
}

void resizeGL(int width, int height){
	if (height <= 0)
		height = 1;

	double aspect = width / height;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	gluOrtho2D(-aspect, aspect, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
}

void updateChildRect(HWND parent){
	GetClientRect(parent, &mainSize);
	childrect.left = mainSize.left + BTNW +MGN * 2;
	childrect.right = mainSize.right - (BTNW + MGN * 3);
	childrect.top = mainSize.top + MGN;
	childrect.bottom = mainSize.bottom - MGN * 2;
}
//Temp debug function
void run(){
	a1.detectOnsets();
	a1.play();
}
