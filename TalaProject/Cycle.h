#pragma once

#include "Beat.h"

class Cycle
{
public:
	Cycle();
	~Cycle();

private:
	int nbeats;
	Beat* beats;
	int interval;
};


