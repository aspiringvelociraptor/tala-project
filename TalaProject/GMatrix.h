#pragma once
#include "GVector.h"

using namespace gvector;
namespace gmatrix{
	template<typename T, const int height, const int width> class GMatrix{
	public:
		inline GMatrix();
		inline GMatrix(GVector<T, height>* cols);
		inline GMatrix(const GMatrix& mat);
		inline GMatrix operator+(const GMatrix& mat);
		inline GMatrix operator-(const GMatrix& mat);
		inline GMatrix operator=(const GMatrix& right);
		static inline GMatrix scale(T x, T y, T z);
		static inline GMatrix scale(T f);
		static inline GMatrix scale(Vec3<T> v);
		static inline GMatrix translate4(T x, T y, T z);
		static inline GMatrix translate3(T x, T y);
		static inline GMatrix translate4(Vec3<T> v);
		static inline GMatrix translate3(Vec3<T> v);
		static inline GMatrix rotater(T x, T y, T z);
		static inline GMatrix rotated(T x, T y, T z);
		inline friend GMatrix operator*(const GMatrix<T, height, width>& left, const T scalar){
			GMatrix res;
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					res.cols[width][height] = left.cols[width][height] * scalar;
				}
			}
			return res;
		}
		inline friend GMatrix operator*(const T scalar, const GMatrix<T, height, width>& right){
			return right * scalar;
		}
		inline friend GMatrix operator/(const GMatrix<T, height, width>& left, const T scalar){
			GMatrix res;
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					res.cols[width][height] = left.cols[width][height] / scalar;
				}
			}
			return res;
		}
		inline friend GMatrix operator/(const T scalar, const GMatrix<T, height, width>& right){
			GMatrix res;
			for (int i = 0; i < width; i++){
				for (int j = 0; j < height; j++){
					res.cols[width][height] = scalar/left.cols[width][height];
				}
			}
			return res;
		}
		inline friend GVector<T, width> operator*(const GVector<T, height>& vec, const GMatrix<T, height, width>& mat){
			T vVal;
			GVector <T, width> res;
			for (int i = 0; i < width; i++){
				vVal = 0;
				for (int j = 0; j < height; j++){
					vVal += vec[j] * mat.cols[i][j];
				}
				res[i] = vVal;
			}
			return res;
		}

		inline friend GVector<T, height> operator*(const GMatrix<T, height, width>& vec, const GVector<T, width>& mat){
			T mVal;
			GVector<T, height> res;
			for (int i = 0; i < height; i++){
				vVal = 0;
				for (int j = 0; j < width; j++){
					vVal += vec[j] * mat.cols[i][j];
				}
				res[i] = vVal;
			}
			return res;
		}

		template<typename T, int hleft, int wh, int wright> inline friend GMatrix operator*(const typename GMatrix<T, hleft, wh>& left, const typename GMatrix<T, wh, wright>& right){
			T mVal;
			GMatrix<T, hleft, wright> res;
			for (int i = 0; i < hleft; i++){
				for (int j = 0; j < wright; w++){
					mVal = 0;
					for (int k = 0; k < wh; k++){
						mVal += left.cols[k][i] * right.cols[j][k];
					}
					res.cols[j][i] = mVal;
				}
			}
		}
	protected:
		GVector cols[width];
		inline void copy(const GMatrix& mat);
	};
	#include "GMatrix.inl"
}


