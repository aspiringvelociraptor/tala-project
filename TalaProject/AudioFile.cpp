#include "stdafx.h"
#include "AudioFile.h"

std::vector<double> AudioFile::onsetbuffer;
AudioFile::AudioFile(char* file){ initialise(file); }
AudioFile::AudioFile(){ initialise(""); }
AudioFile::~AudioFile(){ initialise(""); }

void AudioFile::initialise(char* file){
	strcpy_s(this->file, file); 
	odsdata = (float*)malloc(onsetsds_memneeded(ODS_ODF_RCOMPLEX, 512, 11));
	onsetsds_init(&ods, odsdata, ODS_FFT_FFTW3_HC, ODS_ODF_RCOMPLEX, 512, 11, 44100.f);
}

void AudioFile::play(){
	PlaySoundA(file, NULL, SND_FILENAME | SND_ASYNC);
}

int AudioFile::detectOnsets(){
	using namespace std::placeholders;
	onsetsds_init_audiodata(&odsbuff, &ods, 256);
	int res = onsetsds_process_audiofile(&odsbuff, file, &onsetCallback);
	onsets = onsetbuffer; //copy contents of the buffer into this instance
	onsetbuffer.clear();
	return res;
}

void AudioFile::addOnset(OnsetsDS* ods, double time){
	onsetbuffer.push_back(time);
}

//We must use a static callback function, not a member function
void AudioFile::onsetCallback(OnsetsDS* ods, double time){
	addOnset(ods, time);
}

char* AudioFile::getPath(){ return file; }